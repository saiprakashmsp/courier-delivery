//
//  ProfileViewController.swift
//  msp2
//
//  Created by Mekonda,Sai Prakash on 4/13/17.
//  Copyright © 2017 Mekonda,Sai Prakash. All rights reserved.
//

import UIKit
import Parse

class ProfileViewController: UIViewController {

    
    @IBOutlet weak var usernameLBL: UILabel!
    
    @IBOutlet weak var emailLBL: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
        
    //Causes the view (or one of its embedded text fields) to resign the first responder status.
    func dismissKeyboard() {
    
        view.endEditing(true)
        
    }
    
    let user = PFUser()
    var z: [PFObject] = []
    
    // this fuction that displays the username and details
    override func viewWillAppear(_ animated: Bool)
    {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(ProfileViewController.dismissKeyboard))
        
        tapper.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tapper)
        
        let profile = PFQuery(className: "User")
        profile.whereKeyExists("username")
        profile.whereKeyExists("email")
        
        profile.findObjectsInBackground(block: {
            (objects: [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                self.z = objects!
                for i in self.z {
                    self.usernameLBL.text = "\(i["username"]!)"
                    self.emailLBL.text = "\(i["email"]!)"
                }
            }
            
        })
    }
    
    // display alert message
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // this func is used to delete account
    @IBAction func deleteAcc(_ sender: Any) {
        self.user.deleteInBackground(block: {(success,error) in
            self.displayAlertWithTitle("Deleted!!", message: "Account Successfully deleted")
    })
    }
    
    // func used to logout
    @IBAction func logout(_ sender: Any) {
        PFUser.logOut()
        let login = storyboard?.instantiateViewController(withIdentifier: "first")
        self.present(login!, animated: true, completion:{})
        
    }
        
        
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
