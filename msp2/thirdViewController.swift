//
//  thirdViewController.swift
//  msp2
//
//  Created by Mekonda,Sai Prakash on 2/10/17.
//  Copyright © 2017 Mekonda,Sai Prakash. All rights reserved.
//

import UIKit
import Parse

class thirdViewController: UIViewController {

    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var width: UITextField!
    @IBOutlet weak var length: UITextField!
    @IBOutlet weak var destination: UITextField!
    @IBOutlet weak var wt: UISegmentedControl!
    @IBOutlet weak var ht: UISegmentedControl!
   
    var package : [PFObject] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func wtsegcont(_ sender: Any) {
   
        switch (sender as AnyObject).selectedSegmentIndex {
        case 0:
            var a : String = "\(weight.text!) kgs"
            break;
        case 1:
            var b : String = "\(weight.text!) lbs"
            break;
        default:
            break;
        }
    }
    
    
    @IBAction func htsegcontrol(_ sender: Any) {
        switch (sender as AnyObject).selectedSegmentIndex {
        case 0:
            var a : String = "\(height.text!) cm"
            break;
        case 1:
            var b : String = "\(height.text!) mm"
            break;
        default:
            break;
        }
    }
    
    
    @IBAction func Submit(_ sender: Any) {
        let x = PFObject(className: "Packagedetails")
        x["weight"] = Double(weight.text!)
        x["height"] = Double(height.text!)
        x["width"] = Double(width.text!)
        x["length"] = Double(length.text!)
        x["destination"] = destination.text
        
        x.saveInBackground(block : {(success , error) -> Void in
            print("data transfered")
               self.displayAlertWithTitle("Success!", message:"Package details are successfully transfered")
        })
        
    }
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let tapper = UITapGestureRecognizer(target: self, action:#selector(thirdViewController.dismissKeyboard))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
