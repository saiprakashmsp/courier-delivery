//
//  SecondViewController.swift
//  msp2
//
//  Created by Mekonda,Sai Prakash on 2/10/17.
//  Copyright © 2017 Mekonda,Sai Prakash. All rights reserved.
//

import UIKit
import Parse


class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var firstname: UITextField!
    
    @IBOutlet weak var lastname: UITextField!
    
    @IBOutlet weak var confirmpassword: UITextField!
    
    @IBOutlet weak var phonenumber: UITextField!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    
   // in this function you take the account login credentials
    @IBAction func create(_ sender: Any) {
          let user = PFUser()
        user.username = username.text!
        user.password = password.text!
        user.email = email.text!
        user.setValue(firstname.text, forKey: "firstname")
        user.setValue(lastname.text, forKey: "Lastname")
        
        user.setValue(confirmpassword.text, forKey: "ConfirmPassword")
        user["Phonenumber"] = Int(phonenumber.text!)
        // Signing up using the Parse API
        if (password.text == confirmpassword.text){
        user.signUpInBackground( block: {
            (success, error) -> Void in
            if let error = error as NSError? {
                let errorString = error.userInfo["error"] as? NSString
                // In case something went wrong, use errorString to get the error
                self.displayAlertWithTitle("Something has gone wrong", message:"\(errorString)")
            }
            else
            {
                // Everything went okay
                self.displayAlertWithTitle("Success!", message:"Registration was successful")
                
                
                let emailVerified = user["emailVerified"]
                if emailVerified != nil && (emailVerified as! Bool) == true {
                    // Everything is fine
                }
                else {
                    // The email has not been verified, so logout the user
                    PFUser.logOut()
                }
            }
        })
        }
        else
        {
            self.displayAlertWithTitle("Password Mismatch", message:"Password incorrect!!")
        }
    }
    
    // the tapper guesture recognizer to recognize taps
    override func viewWillAppear(_ animated: Bool) {
        let tapper = UITapGestureRecognizer(target: self, action:#selector(SecondViewController.dismissKeyboard))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }
    
    //Causes the view (or one of its embedded text fields) to resign the first responder status.
    func dismissKeyboard() {
      
        view.endEditing(true)
    }
    
    //this is an pop up window 
        func displayAlertWithTitle(_ title:String, message:String){
            let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(defaultAction)
            self.present(alert, animated: true, completion: nil)
        }
}

