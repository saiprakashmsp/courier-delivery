//
//  Packagedetails.swift
//  msp2
//
//  Created by Mekonda,Sai Prakash on 4/13/17.
//  Copyright © 2017 Mekonda,Sai Prakash. All rights reserved.
//

import Foundation
import Parse
import Bolts

class Packagedetails:PFObject, PFSubclassing {

    @NSManaged var weight: Double
    @NSManaged var height: Double
    @NSManaged var width: Double
    @NSManaged var length: Double
    @NSManaged var destination: String
    
    static func parseClassName() -> String {
        return "Packagedetails"
    }
}
