//
//  FirstViewController.swift
//  msp2
//
//  Created by Mekonda,Sai Prakash on 2/10/17.
//  Copyright © 2017 Mekonda,Sai Prakash. All rights reserved.
//

import UIKit
import Parse

class FirstViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // this is used to alert the screen with pop up
    func alert(message: NSString, title: NSString) {
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // log in page using credentials
    @IBAction func Login(_ sender: Any) {
        let username = self.username.text!
        let password = self.password.text!
        
        PFUser.logInWithUsername(inBackground: username, password: password, block: {(user, error) -> Void in
            if let error = error as NSError? {
                let errorString = error.userInfo["error"] as? NSString
                self.alert(message: errorString!, title: "Error")
            }
            else
            {
                self.performSegue(withIdentifier: "login", sender: self)
            }
            print("data was transfered")
        })
    }
    
    // Tapper guesture to find the no of taps
    override func viewWillAppear(_ animated: Bool) {
        
        let tapper = UITapGestureRecognizer(target: self, action:#selector(FirstViewController.dismissKeyboard))
        
        tapper.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tapper)
        
    }
    
    // this function is used to dismiss keyboard
    func dismissKeyboard() {
        
        view.endEditing(true)
        
    }
    
    @IBAction func unwindToA(sender: UIStoryboardSegue)
    {
        
    }

}

