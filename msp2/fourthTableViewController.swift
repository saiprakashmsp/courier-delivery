//
//  fourthTableViewController.swift
//  msp2
//
//  Created by Mekonda,Sai Prakash on 3/6/17.
//  Copyright © 2017 Mekonda,Sai Prakash. All rights reserved.
//

import UIKit

class fourthTableViewController: UITableViewController {
    
    var msp:[String] = ["FedEx Courier","UPS Courier","Quick Courier"]
    var images:[UIImage] = [#imageLiteral(resourceName: "fedex"),#imageLiteral(resourceName: "ups"),#imageLiteral(resourceName: "quick")]
    var price:[Double] = [10.0,9.30,10.50,8.0,9.0,10.0,7.0,8.0,8.50,6.0,7.50,7.0]
    var date:[String] = ["Estimated delivery 1 days","Estimated delivery 2 days","Estimated delivery 4 days","Estimated delivery 5 days"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
     return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return msp.count
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let tapper = UITapGestureRecognizer(target: self, action:#selector(fourthTableViewController.dismissKeyboard))
        
        tapper.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tapper)
        
    }
    func dismissKeyboard() {
        
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        
        view.endEditing(true)
        
    }
    
    
// used to populate table view
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rep", for: indexPath)
        
        if(indexPath.section == 0){
            cell.textLabel?.text = msp[indexPath.row]
            cell.imageView?.image = images[indexPath.row]
            cell.detailTextLabel?.text = String("$\(price[indexPath.row])")
        }
        if(indexPath.section == 1){
            cell.textLabel?.text = msp[indexPath.row]
            cell.imageView?.image = images[indexPath.row]
            cell.detailTextLabel?.text = String("$\(price[indexPath.row+3])")
        }
        if(indexPath.section == 2){
            cell.textLabel?.text = msp[indexPath.row]
            cell.imageView?.image = images[indexPath.row]
            cell.detailTextLabel?.text = String("$\(price[indexPath.row+6])")
        }
        if(indexPath.section == 3){
            cell.textLabel?.text = msp[indexPath.row]
            cell.imageView?.image = images[indexPath.row]
            cell.detailTextLabel?.text = String("$\(price[indexPath.row+9])")
        }
        
        if (indexPath.row == 0){
            cell.backgroundColor = UIColor.gray
        }
        if (indexPath.row == 1){
            cell.backgroundColor = UIColor.white
        }
        if (indexPath.row == 2){
            cell.backgroundColor = UIColor.cyan
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return date[section]
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
